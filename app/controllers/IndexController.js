'use strict';

module.exports = {

    index(req, res, next) {
        const model = res.baseModel;
        res.render('index', model);
    },

    inauguration(req, res, next) {
        const model = res.baseModel;
        res.render('inauguration', model);
    },

};

