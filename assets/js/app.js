function initialize(name, options) {
  if (name === 'back-to-top') {
    var amountScrolled = 300;

    $(window).scroll(function() {
      $('a.back-to-top')['fade' + ($(window).scrollTop() > amountScrolled ? 'In' : 'Out')]('slow');
    });

    $('a.back-to-top').click(function() {
      $('body, html').animate({
        scrollTop: 0
      }, 700);
    });
  }
}