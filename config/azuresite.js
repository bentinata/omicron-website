module.exports = _.merge(require('./'), {

    local: true,
    port: process.env.PORT || 80,

    // database
    mongodb: {
        host: 'ds048878.mongolab.com',
        port: '48878',
        dbname: 'ilkom',
        username: 'bentinata',
        password: 'ilkom',
    },

    // swig
    swig: {
        cache: false,
    },
});